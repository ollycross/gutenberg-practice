<?php

namespace OllyOllyOlly\OllyBlock;

use Stringy\Stringy;
use WP_Post;

/**
 * Class Plugin
 * @package OllyOllyOlly\OllyBlock
 */
class Plugin
{

    /**
     * Namespace for blocks created by the plugin
     */
    public const BLOCK_NAMESPACE = 'olly-block';

    /**
     * Initialises plugin
     *
     * @return static
     */
    public static function init()
    {
        $plugin = new static();
        $plugin->setup();

        return $plugin;
    }

    /**
     * Sets up plugin actions and filters
     */
    public function setup()
    {
        add_filter('block_categories', [ $this, 'registerBlockCategories' ], 10, 2);
        add_action('init', [ $this, 'registerBlockAssets' ]);
    }

    /**
     * Registers block categories for plugin
     *
     * @param array   $categories The unfiltered categories.
     * @param WP_Post $post The current post.
     *
     * @return array
     */
    public function registerBlockCategories($categories, $post)
    {
        if ('post' !== $post->post_type) {
            return $categories;
        }

        return array_merge(
            $categories,
            [
                [
                    'slug'  => 'ollyollyolly',
                    'title' => __('OllyOllyOlly', 'ollyollyolly'),
                    'icon'  => 'marker',
                ],
            ]
        );
    }

    /**
     * Enqueue Gutenberg block assets for both frontend + backend.
     *
     * Assets enqueued:
     * 1. blocks.style.build.css - Frontend + Backend.
     * 2. blocks.build.js - Backend.
     * 3. blocks.editor.build.css - Backend.
     *
     * @uses {wp-blocks} for block type registration & related functions.
     * @uses {wp-element} for WP Element abstraction — structure of blocks.
     * @uses {wp-i18n} to internationalize the block's text.
     * @uses {wp-editor} for WP editor styles.
     * @since 1.0.0
     */
    public function registerBlockAssets()
    {
        // Register block styles for both frontend + backend.
        wp_register_style(
            'olly_block-cgb-style-css',
            plugins_url('dist/blocks.style.build.css', dirname(__FILE__)),
            is_admin() ? [ 'wp-editor' ] : null,
            filemtime(plugin_dir_path(__DIR__) . 'dist/blocks.style.build.css')
        );

        // Register block editor script for backend.
        wp_register_script(
            'olly_block-cgb-block-js',
            plugins_url('/dist/blocks.build.js', dirname(__FILE__)),
            [ 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor' ],
            filemtime(plugin_dir_path(__DIR__) . 'dist/blocks.build.js'),
            true // Enqueue the script in the footer.
        );

        // Register block editor styles for backend.
        wp_register_style(
            'olly_block-cgb-block-editor-css', // Handle.
            plugins_url('dist/blocks.editor.build.css', dirname(__FILE__)),
            [ 'wp-edit-blocks' ],
            filemtime(plugin_dir_path(__DIR__) . 'dist/blocks.editor.build.css')
        );

        // WP Localized globals. Use dynamic PHP stuff in JavaScript via `cgbGlobal` object.
        wp_localize_script(
            'olly_block-cgb-block-js',
            'cgbGlobal',
            [
                'pluginDirPath' => plugin_dir_path(__DIR__),
                'pluginDirUrl'  => plugin_dir_url(__DIR__),
            ]
        );

        /**
         * Register Gutenberg block on server-side.
         *
         * Register the block on server-side to ensure that the block
         * scripts and styles for both frontend and backend are
         * enqueued when the editor loads.
         *
         * @link https://wordpress.org/gutenberg/handbook/blocks/writing-your-first-block-type#enqueuing-block-scripts
         * @since 1.16.0
         */
        foreach (
            [
            'static-editable',
            'static-uneditable',
            'dynamic-internal',
            'dynamic-external',
            ] as $blockType
        ) {
            $args = [
                    'style'         => 'olly_block-cgb-style-css',
                    'editor_script' => 'olly_block-cgb-block-js',
                    'editor_style'  => 'olly_block-cgb-block-editor-css',
            ];

            $class = sprintf(
                '%s\\ViewController\\%s',
                __NAMESPACE__,
                ( new Stringy($blockType) )->camelize()->upperCaseFirst()
            );
            if (class_exists($class)) {
                $viewController = new $class($blockType);
                $args['render_callback'] = [$viewController, 'render'];
            }

            register_block_type(
                sprintf('%s/%s', self::BLOCK_NAMESPACE, $blockType),
                $args
            );
        }
    }

    // Hook: Block assets.
}
