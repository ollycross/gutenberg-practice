<?php

namespace OllyOllyOlly\OllyBlock;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

abstract class ViewController
{
    private $twig;
    public string $blockType;

    public function render(array $attributes = [], string $content = ''): string
    {
        $context = array_merge($attributes, ['content' => $content]);

        return $this->twigRender($context);
    }

    protected function twigRender(array $context = [], $template = 'block.twig'): string
    {
        try {
            return $this->getTwig()->render(
                $template,
                $context
            );
        } catch (\Exception $e) {
            trigger_error($e->getMessage(), E_USER_WARNING);
            return '';
        }
    }

    public function __construct(string $blockName)
    {
        $this->blockType = $blockName;
    }

    private function getTwig(): Environment
    {
        if (!$this->twig) {
            $this->initTwig();
        }

        return $this->twig;
    }

    private function getTemplateDir(): string
    {
        return sprintf('%ssrc/blocks/%s/views', OLLYOLLYOLLY_BLOCK_DIR, $this->blockType);
    }

    private function initTwig(): void
    {
        $loader = new FilesystemLoader($this->getTemplateDir());
        $this->twig = new Environment($loader);
    }
}
