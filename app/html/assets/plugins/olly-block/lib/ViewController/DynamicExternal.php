<?php

namespace OllyOllyOlly\OllyBlock\ViewController;

use OllyOllyOlly\OllyBlock\Plugin;
use Timber\Timber;

class DynamicExternal extends \OllyOllyOlly\OllyBlock\ViewController
{

    private const FEED_TEMPLATE = 'https://www.youtube.com/feeds/videos.xml?channel_id=%s';
    private const LINK_TEMPLATE = 'https://www.youtube.com/feeds/videos.xml?channel_id=%s';

    private function getChannel(string $channel, int $limit = 10): array
    {
        global $wp_embed;

        $url = sprintf(self::FEED_TEMPLATE, $channel);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        $data = curl_exec($ch);
        curl_close($ch);

        $xml = simplexml_load_string($data);

        $videos = [];
        foreach ($xml->entry as $entry) {
            $content = sprintf('[embed]%s[/embed]', (string) $entry->link['href']);
            $videos[] = $wp_embed->run_shortcode($content);
            if (count($videos) === $limit) {
                break;
            }
        }

        return [
            (string) $xml->title,
            $videos,
        ];
    }

    public function render(array $attributes = [], string $content = ''): string
    {
        $channel = $attributes['channel'] ?? '';
        $numVideos = $attributes['numVideos'] ?? 5;

        if (!$channel) {
            return '';
        }

        [$channelTitle, $videos] = $this->getChannel($channel, $numVideos);

        $context = [
            'channelTitle' => $channelTitle,
            'videos' => $videos,
            'numVideos' => $numVideos
        ];

        return $this->twigRender($context);
    }
}
