<?php

namespace OllyOllyOlly\OllyBlock\ViewController;

use Timber\Timber;

class DynamicInternal extends \OllyOllyOlly\OllyBlock\ViewController
{
    public function render(array $attributes = [], string $content = ''): string
    {
        ['numPosts' => $numPosts] = $attributes;

        $posts = Timber::get_posts([
            'numberposts' => $numPosts
        ]);

        $context = [
            'posts' => $posts,
            'numPosts' => $numPosts
        ];

        return $this->twigRender($context);
    }
}
