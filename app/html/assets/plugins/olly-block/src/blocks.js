/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */

import './blocks/static-uneditable/block';
import './blocks/static-editable/block';
import './blocks/dynamic-internal/block';
import './blocks/dynamic-external/block';
