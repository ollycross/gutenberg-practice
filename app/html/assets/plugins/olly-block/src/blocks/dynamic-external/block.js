import { NAMESPACE } from '../../common';
import BlockHelper from '../../utils/block-helper';

const { TextControl } = wp.components;
const { registerBlockType } = wp.blocks;
const { __ } = wp.i18n;

const NAME = 'dynamic-external';

const blockHelper = new BlockHelper( NAMESPACE, NAME );

const getContent = ( variableContent ) => (
	<div className={ blockHelper.getClassName() }>
		<h2>This block will pull the latest videos from a Youtube channel</h2>
		{ variableContent }
	</div>
);

registerBlockType( blockHelper.getBlockName(), {
	title: __( 'Dynamic block - Youtube feed' ),
	icon: 'video-alt',
	category: 'ollyollyolly',
	attributes: {
		numVideos: {
			type: 'int',
			selector: blockHelper.getSelector( 'num-posts' ),
		},
		channel: {
			type: 'text',
			selector: blockHelper.getSelector( 'channel' ),
		},
	},
	edit: ( { attributes: { numVideos = 5, channel = 'UCYK1TyKyMxyDQU8c6zF8ltg' }, setAttributes } ) =>

		getContent(
			<div>
				<TextControl
					label={ __( 'Channel ID' ) }
					type="text"
					value={ channel }
					onChange={ ( ( newChannel ) => setAttributes( { channel: newChannel } ) ) }
				/>
				<TextControl
					label={ __( 'Number of videos' ) }
					type="number"
					value={ numVideos }
					onChange={ ( ( newNumVideos ) => setAttributes( { numVideos: newNumVideos } ) ) }
					min={ 1 }
					max={ 10 }
					step={ 1 }
				/>
			</div>
		),
	save: () => null,
} );
