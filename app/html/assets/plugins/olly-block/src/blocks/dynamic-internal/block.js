import { NAMESPACE } from '../../common';
import BlockHelper from '../../utils/block-helper';

const { TextControl } = wp.components;
const { registerBlockType } = wp.blocks;
const { __ } = wp.i18n;

const NAME = 'dynamic-internal';

const blockHelper = new BlockHelper( NAMESPACE, NAME );

const getContent = ( variableContent ) => (
	<div className={ blockHelper.getClassName() }>
		<h2>This block will pull the latest posts from Wordpress</h2>
		{ variableContent }
	</div>
);

registerBlockType( blockHelper.getBlockName(), {
	title: __( 'Dynamic block - posts' ),
	icon: 'welcome-write-blog',
	category: 'ollyollyolly',
	attributes: {
		numPosts: {
			type: 'int',
			selector: blockHelper.getSelector( 'num-posts' ),
		},
	},
	edit: ( { attributes: { numPosts = 5 }, setAttributes } ) =>

		getContent(
			<TextControl
				label={ __( 'Number of posts' ) }
				type="number"
				value={ numPosts }
				onChange={ ( ( newNumPosts ) => setAttributes( { numPosts: newNumPosts } ) ) }
				min={ 1 }
				max={ 10 }
				step={ 1 }
			/>
		),
	save: () => null,
} );
