import { NAMESPACE } from '../../common';
import BlockHelper from '../../utils/block-helper';
import ImageSelector from '../../components/image-selector';

const { registerBlockType } = wp.blocks;
const { __ } = wp.i18n;
const { RichText, MediaUpload } = wp.blockEditor;

const NAME = 'static-editable';

const blockHelper = new BlockHelper( NAMESPACE, NAME );

const getContent = ( variableContent ) => (
	<div className={ blockHelper.getClassName() }>
		<h2>The text below is editable</h2>
		{ variableContent }
	</div>
);

registerBlockType( blockHelper.getBlockName(), {
	title: __( 'Static, editable block' ),
	icon: 'heart',
	category: 'ollyollyolly',
	attributes: {
		body: {
			type: 'array',
			source: 'children',
			selector: blockHelper.getSelector( 'body' ),
		},
		imageUrl: {
			attribute: 'src',
			selector: blockHelper.getSelector( 'image' ),
		},
		imageAlt: {
			attribute: 'alt',
			selector: blockHelper.getSelector( 'image' ),
		},
		imageId: {
		},
	},
	edit: ( { attributes: { body, imageId, imageUrl, imageAlt }, setAttributes } ) =>

		getContent(
			<div>
				<MediaUpload
					onSelect={ ( { id, alt, url } ) => setAttributes( { imageAlt: alt, imageUrl: url, imageId: id } ) }
					type="image"
					value={ imageId }
					render={ ( { open } ) => ( <ImageSelector imageUrl={ imageUrl } imageAlt={ imageAlt } opener={ open } /> ) }
				/>
				<RichText
					onChange={ content => setAttributes( { body: content } ) }
					value={ body }
					multiline="p"
					placeholder={ __( 'Enter some text here' ) }
				/>
			</div>
		),
	save: ( { attributes: { body, imageUrl, imageAlt } } ) => getContent( (
		<div>
			{ imageUrl ? ( <img className={ blockHelper.getClassName( 'image' ) } src={ imageUrl } alt={ imageAlt } aria-hidden={ ! imageAlt } /> ) : null }
			{ body ? ( <div className={ blockHelper.getClassName( 'body' ) }>{ body }</div> ) : null }
		</div>
	) ),
} );
