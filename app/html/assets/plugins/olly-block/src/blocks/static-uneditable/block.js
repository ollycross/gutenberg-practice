import { NAMESPACE } from '../../common';
import BlockHelper from '../../utils/block-helper';

const { registerBlockType } = wp.blocks;
const { __ } = wp.i18n;
const NAME = 'static-uneditable';

const blockHelper = new BlockHelper( NAMESPACE, NAME );

const content = (
	<div className={ blockHelper.getClassName() }>
		<h2>The text below is not editable</h2>
		<p>{ __( 'This block is static and you can\'t change anything about it.  Good times.' ) } </p>
	</div>
);

registerBlockType( blockHelper.getBlockName(), {
	title: __( 'Static, uneditable block' ),
	icon: 'smiley',
	category: 'ollyollyolly',
	edit: () => content,
	save: () => content,
} );
