const { Button } = wp.components;
const { __ } = wp.i18n;

const ImageSelector = ( { opener, imageUrl, imageAlt } ) => (
	<div>
		{ imageUrl ? ( <img
			alt={ imageAlt }
			src={ imageUrl }
			className="image"
		/> ) : null }
		<div className="button-container">
			<Button
				onClick={ opener }
				className="button button-large"
			>
				{ imageUrl ? __( 'Change image' ) : __( 'Select image' ) }
			</Button>
		</div>
	</div>
);

export default ImageSelector;
