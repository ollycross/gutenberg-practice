class BlockHelper {
	constructor( namespace, name ) {
		this.namespace = namespace;
		this.name = name;
	}

	getClassName = ( element = '' ) => [ this.namespace, this.name, element ].filter( val => !! val ).join( '__' );
	getSelector = ( element = '' ) => `.${ this.getClassName( element ) }`;

	getBlockName() {
		return `${ this.namespace }/${ this.name }`;
	}
}

export default BlockHelper;
