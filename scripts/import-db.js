const commander = require('commander');
const { execSync } = require('child_process');
require('dotenv').config();
const fs = require('fs');

commander
  .option('-r, --remote <user@host>', 'import to remote host')
  .option('-d, --debug', 'debug mode')
  .option('-f, --file <file>', 'SQL Dump file', './docker/mysql/latest.sql');
commander.parse(process.argv);

let user;
let password;
let schema;

if (commander.debug) {
  console.log(commander.opts());
}

if (!fs.existsSync(commander.file)) {
  throw new Error(`File not found at '${commander.file}'`);
}

async function importLocal() {
  const container = 'mariadb';
  ({
    MYSQL_USER: user,
    MYSQL_PASSWORD: password,
    MYSQL_SCHEMA: schema,
  } = process.env);

  return execSync(
    `docker exec -ie MYSQL_PWD=${password} ${container} mysql -u ${user} ${schema} < ${commander.file}`,
  ).toString();
}

importLocal()
  .then(console.log)
  .catch(console.error);
